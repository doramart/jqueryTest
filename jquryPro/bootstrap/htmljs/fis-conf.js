// fis.match('::packager', {
//   spriter: fis.plugin('csssprites')
// });

fis.match('*.{js,css,png,gif}', {
    useHash: true // 开启 md5 戳
});


fis.match('*.js', {
  optimizer: fis.plugin('uglify-js'),
   release : '/static/js$0'
});

fis.match('*.css', {
  useSprite: true,
  optimizer: fis.plugin('clean-css'),
   release : '/static/css$0'
});

fis.match('*.png', {
  optimizer: fis.plugin('png-compressor'),
  release : '/static/pic/$0'
});

// fis.match('*.js', {
//   useHash: false
// });

// fis.match('*.css', {
//   useHash: false
// });

// fis.match('*.png', {
//   useHash: false
// });

// fis.match('*.{js,css,png}', {
//   useHash: true
// });