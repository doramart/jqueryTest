/**
 * Created by Administrator on 2016/3/15.
 */
'use strict';


//{
//    var a =12;
//    let b = 15;
//
//}
//
//console.log(a,b);

//泄密
//for(let i=0;i<5;i++){}
//console.log(i);

//提升
//function f(){
//    console.log(a);
//    let a = 12;
//}
//f();

//临时失效区
//var a=12;
//function f(){
//    console.log(a);
//    let a;
//}

//{
//    let a;
//    let a;
//}
//代替立即执行匿名函数
//var config = (function(){
//    var config = [];
//    config.push(1);
//    config.push(2);
//    config.push(3);
//    return config;
//})();
//
//let config;
//{
//    config = [];
//    config.push(1);
//    config.push(2);
//    config.push(3);
//}

//函数不被提升
//function a(){}
//
//function f(){
//    a();
//    if(false){
//        return a(){}
//    }
//}
//
//f()


//实例
//var arr = [];
//function f(){
//    for(var i=0;i<5;i++){
//        arr.push(function(){
//            console.log(i);
//        })
//    }
//}
//
//f();
//arr[2]();

//const使用 对屋里内存地址不可更改
const a = {
    name : 'dora'
};

a.name = 'xiaoshen';
a.email = '33e@qq.com';
//彻底冻结
Object.freeze(a);
a.name = 'huiping';

console.log(a);