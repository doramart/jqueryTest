/**
 * Created by Administrator on 2016/3/15.
 */

function User(name,age){
    this.name = name;
    this.age = age;
}

//静态方法
User.getClassName = function(){
    return 'User';
};

User.prototype.changeName = function(name){
  this.name = name;
};

User.prototype.changeAge = function(age){
    this.age = age;
};

//取值函数
Object.defineProperty(User.prototype,'info',{
    get(){
        return 'name:' + this.name + 'age:' + this.age
    }
});

//子类
function Manager(name,age,pwd){
    User.call(this,name,age);
    this.psd = pwd;
}

//继承静态方法
Manager.__proto__ = User;

//继承prototype方法
Manager.prototype = User.prototype;

//添加新方法
Manager.prototype.changePassWord = function(pwd){
    this.pwd = pwd;
};

var manager  = new Manager('dora',25,'222');

manager.changeName('doramart');
console.log(manager.info);