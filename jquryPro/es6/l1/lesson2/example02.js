/**
 * Created by Administrator on 2016/3/15.
 */
'use strict';
//function User(name,age){
//    this.name = name;
//    this.age = age;
//}


class User{
    constructor(name,age){
        this.name = name;
        this.age = age;
    }

    //静态方法
    //User.getClassName = function(){
    //return 'User';
    static getClassName(){
        return 'User';
    }

    changeName(name){
        this.name = name;
    }

    changeAge(age){
        this.age = age;
    }

    get info(){
        return 'name:' + this.name + ' | age:' + this.age
    }

}

//子类
//function Manager(name,age,pwd){
//    User.call(this,name,age);
//    this.psd = pwd;
//}
class Manager extends User{
    constructor(name,age,pwd){
        super(name,age);
        this.pwd = pwd;
    }

    changePassWord(pwd){
        this.pwd = pwd;
    }

    get info(){
        return super.info;
    }
}
//继承静态方法
//Manager.__proto__ = User;
//
////继承prototype方法
//Manager.prototype = User.prototype;

//添加新方法
//Manager.prototype.changePassWord = function(pwd){
//    this.pwd = pwd;
//};

var manager  = new Manager('dora',25,'222');

manager.changeName('doramart');
console.log(manager.info);

class I extends User{


}

var me  = new I('dora',266);
console.log(me)