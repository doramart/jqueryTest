/**
 * Created by Administrator on 2016/3/15.
 */
//function asyncFun(a,b,cb){
//    setTimeout(function(){
//        cb(a+b);
//    },200)
//}
//
//asyncFun(1,2,function(result){
//    if(result > 2){
//
//    }
//});
//
//console.log(2);

function asyncFun(a,b){
    return new Promise(function(resolve,reject){
        if(typeof a  != 'number' || typeof a  != 'number'){
            reject(new Error('类型不正确'));
        }
        setTimeout(function(){
            resolve(a+b);
        },200)
    });
}

asyncFun(1,'2')
    .then(function(result){
        if(result > 2){
            return asyncFun(result,2);
        }
    },function(err1){
        console.log('first:'+err1)
    })
    .then(function(result){
        if(result > 4){
            console.log('ok');
        }
    })
    .catch(function(err){
        console.log(err)
    });