/**
 * Created by Administrator on 2016/3/16.
 */
'use strict';

class User{

    constructor(name,password){
        this.name = name;
        this.password = password;
    }

    validateName(cb){
        let name = this.name;
        return new Promise(function(resolve,reject){
            setTimeout(function(){
                if(name === 'dora'){
                    resolve('success name');
                }else{
                    reject('error name');
                }
            },200);

        })
    }

    validatePwd(cb){
        let password = this.password;
        return new Promise(function(resolve,reject){
            setTimeout(function(){
                if(password === '123'){
                    resolve('success password');
                }else{
                    reject('error password');
                }
            },200);

        })
    }

}

const user = new User('dora1','123');
user.validateName().then(function(result){
    return user.validatePwd();
},function(err1){
    console.log('error1:'+err1);
    return;
}).then(function(result1){
    console.log(result1);
}).catch(function(err2){
    console.log('error2:'+err2);
});