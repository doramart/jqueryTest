/**
 * Created by Administrator on 2016/3/16.
 */
function asyncFun(a,b){
    return new Promise(function(resolve,reject){
        if(typeof a  != 'number' || typeof a  != 'number'){
            reject(new Error('类型不正确'));
        }
        setTimeout(function(){
            resolve(a+b);
        },200)
    });
}

const resultList = [];
//asyncFun(1,2)
//.then(function(result){
//    resultList.push(result);
//    return asyncFun(2,3);
//})
//.then(function(result){
//    resultList.push(result)
//});

//var promise = Promise.all([asyncFun(1,2),asyncFun(2,3)]);
var promise = Promise.race([asyncFun(1,2),asyncFun(2,3)]);

promise.then(function(result){
    console.log(result);
});

