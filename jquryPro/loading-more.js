
//加载更多html
function rzcMobilePageHtml(){
    return "<div attribute-name='loading-btn'></div><div class='loading-icon'></div>";
}

/*
 * @loading more
 */
function rzcMobilePage(json){
	this.html = rzcMobilePageHtml();
	this.content = json.content;  //内容容器
	this.dataurl = json.dataurl; //数据请求地址
    this.number = json.number || 10;  //每页显示行数
	this.theme = json.theme || 'A'; //加载更多皮肤
	
	this.init();
}

/*
 * @function
 */
rzcMobilePage.prototype = {
	pagenumber : 1,
	obj : "",
	loadingh : 0,
	init : function(){
		var _getEvent = getEvent();
		var _this = this;
		$("body").append(_this.html);
		_this.obj =  $("body").find("[attribute-name='loading-btn']");
		_this.obj.addClass("loading-more-" + _this.theme);
		_this.loadingh = _this.obj.height();
		
		_this.appendData();
		
		//绑定事件--加载更多按钮点击事件
		$(_this.obj).bind("click",function(){
			_this.pagenumber ++;  
			_this.appendData();
		});
		
		//绑定事件--移动端触摸上滑加载事件
		$(_this.content).on(_getEvent.eventStar,function(e){
			//触发条件是加载更多按钮位于浏览器底部
			if($(_this.obj).next().length > 0 && $(_this.obj).offset().top + $(_this.obj).outerHeight() - 1 <= $(_this.obj).next().offset().top){
				ev = e || window.event;
				_this.moveStart(ev);
			}
		});
	},
	
	//加载更多按钮位于底部时触摸滑动开始
	moveStart : function(e){
		var _getEvent = getEvent();
		var _this = this;
		var _startY = e.pageY || event.targetTouches[0].pageY;
		var _startT = $(document).scrollTop();
		var _endY;
		var _isup = false;
		var _move = function(e){
			var _moveto = 0;
			_endY = e.pageY || event.targetTouches[0].pageY;
			if(_endY < _startY){  //只有向上触摸滑动时才执行操作
				e.preventDefault();
				_isup = true;
				_moveto = (_startY - _endY)/2;
				$(_this.obj).next().height(_moveto);
				$(_this.obj).height(_this.loadingh+_moveto);
				$(document).scrollTop(_startT + _moveto);
			}
		}
		var _movestop = function(){
			if(_isup){  //向上触摸滑动
				if($(_this.obj).next().height() > 35){  //向上触摸滑动至少35px大小才执行加载
					_this.obj.html("正在努力加载...");
					$(_this.obj).animate({height:_this.loadingh + 35},200);
					$(_this.obj).next().animate({height:35},200,function(){
						$(_this.obj).click();
					});
				}else{
					$(_this.obj).animate({height:_this.loadingh},200);
					$(_this.obj).next().animate({height:0},200);
				}
			}
			$(document).off(_getEvent.eventMove);
			$(document).off(_getEvent.eventEnd);
		}
		$(document).on(_getEvent.eventMove,_move);
		$(document).on(_getEvent.eventEnd,_movestop);
	},
	
	//加载数据
	appendData :function(){
		var _this = this;
		var _url = "";
		if(_this.dataurl.indexOf("?") >= 0){
			_url = _this.dataurl + "&pnum=" + _this.pagenumber + "&num=" + _this.number;
		}else{
			_url = _this.dataurl + "?pnum=" + _this.pagenumber + "&num=" + _this.number;
		}
		$.ajax({
			type: "POST",
			url: _url,
			data: {},
			dataType: "html",
			beforeSend : function(){
				if(_this.pagenumber == 1){
					$(_this.obj).hide();
					addLoading(_this.content);
				}else{
					_this.obj.html("正在努力加载...");
				}
			},
			success: function(msg){
				msg = eval('('+msg+')');  //msg数据示例：{content:"要新加载的数据",state:"返回的数据是不是最后的数据"}
				setTimeout(function(){
				$(_this.content).append(msg.content);
				if($(_this.content).find(".loading").length > 0){
					removeLoading(_this.content);
				}
				if(msg.state){
					$(_this.obj).show();
					_this.obj.html("加载更多...");
					$(_this.obj).animate({height:_this.loadingh},200);
					$(_this.obj).next().animate({height:0},200);
				}else{
					$(_this.obj).next().remove();
					$(_this.obj).remove();
				}
				},2000);
			},
			error: function(error) {
				if($(_this.content).find(".loading").length > 0){
					removeLoading(_this.content);
				}
            	_this.obj.html("<span style='color:#F00;'>加载失败...</span>");
            }
		 })
	}
}