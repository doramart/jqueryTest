/**
 * Created by Administrator on 2015/7/7.
 */
$(function(){
    var _scrollBar = $(".scrollBar");
    var _container = $(".container");
    var _content = $(".content");

    var n = $(_container).height()/$(_content).height(); // 长度比例

//    var oldInnerHeight = $(_inner).height()
    $(_scrollBar).css('height',$(_scrollBar).height()*n + 'px');

    var n1 = ($(_content).height() - $(_container).height())/($(_container).height() - $(_scrollBar).height());

    var movestart = function(e){

        var _x = e.pageX;
        var _y = e.pageY;

        var _startX = $(_scrollBar).offset().left;
        var _startY = $(_scrollBar).offset().top;

        var move = function(e){
            var _right = 0;
//            鼠标移动的距离加上之前相对于外框的高度
            var _top = e.pageY - _y + _startY -  _container.offset().top;

            if(_top < 0){
                _top = 0;
            }

            if(_top > _container.height() - _scrollBar.height()){
                _top = _container.height() - _scrollBar.height();
            }

            $(_scrollBar).css({right:_right,top:_top});

            $(_content).css({top:- _top * n1});


        };

        var movestop = function(){
            $(document).unbind("mousemove");
            $(document).unbind("mouseup");
        };

        $(document).bind("mousemove",move);
        $(document).bind("mouseup",movestop);
    };


    $(document).bind("mousedown",movestart)
})