/**
 * Created by Administrator on 2015/7/15.
 */
/** Event handler for mouse wheel event.
 *鼠标滚动事件
 */
$(function(){


    var wheel = function(event) {
        var delta = 0;
        if (!event) /* For IE. */
            event = window.event;
        if (event.wheelDelta) { /* IE/Opera. */
            delta = event.wheelDelta / 120;
        } else if (event.detail) {
            /** Mozilla case. */
            /** In Mozilla, sign of delta is different than in IE.
             * Also, delta is multiple of 3.
             */
            delta = -event.detail / 3;
        }
        /** If delta is nonzero, handle it.
         * Basically, delta is now positive if wheel was scrolled up,
         * and negative, if wheel was scrolled down.
         */
        if (delta)
            handle(delta);
        /** Prevent default actions caused by mouse wheel.
         * That might be ugly, but we handle scrolls somehow
         * anyway, so don't bother here..
         */
        if (event.preventDefault)
            event.preventDefault();
        event.returnValue = false;
    }

    /** Initialization code.
     * If you use your own event management code, change it as required.
     */

    if (window.addEventListener) {
        /** DOMMouseScroll is for mozilla. */
        window.addEventListener('DOMMouseScroll', wheel, false);
    }
    /** IE/Opera. */
    window.onmousewheel = document.onmousewheel = wheel;

    /** This is high-level function.
     * It must react to delta being more/less than zero.
     */
    var handle = function(delta) {
        var random_num = Math.floor((Math.random() * 100) + 50);
        if (delta < 0) {
            // alert("鼠标滑轮向下滚动：" + delta + "次！"); // 1
            $("#mScroll").val("鼠标滑轮向下滚动：" + delta + "次！");
            return;
        } else {
            // alert("鼠标滑轮向上滚动：" + delta + "次！"); // -1
            $("#mScroll").val("鼠标滑轮向上滚动：" + delta + "次！");
            return;
        }
    }
})
