/**
 * Created by Administrator on 2015/7/17.
 */

//1.直接给jquery添加全局函数
jQuery.myAlert = function(){
    alert('我是调用插件弹出的警告框');
};


//2.用 extend() 方法

jQuery.extend({
    myAlert:function(){
        alert('ok')
    },
    myAlert2:function(){
        alert('ok1')
    }
});

//3、使用命名空间

jQuery.dora = {
    myAlert:function(){
        alert('ok')
    }
};