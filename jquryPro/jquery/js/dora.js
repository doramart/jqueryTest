// JavaScript Document

(function($){

	// 全局函数添加
	 $.doraMathUtils = {
		// 求和
		sum :  function(array){
			
			var total = 0;
			$.each(array, function(index, val) {
				 /* iterate through array or object */

				 var num1 = $.trim(val);
				 var newNum = parseFloat(num1) || 0;

				 total += newNum
			});

			return total;
		},


		// 求平均值

		averageValue :  function(array)
		{
			if ($.isArray(array)) { 
		      return $.doraMathUtils.sum(array) / array.length; 
		    } 
		    return '';
			
		},

		// 字符长度截取
		cutText : function(txt,num)
	       {
	            if (num<=0) 
	            {
	                alert("参数错误，请重试");
	                return txt;
	            };
	            var newtxt = txt.substring(0,num)+"...";
	            return newtxt;
      	 }
	};



	// 对象方法添加（可以连缀）
	$.fn.swapClass = function(class1, class2) { 
	    return this.each(function() { 
	      var $element = $(this); 
	      if ($element.hasClass(class1)) { 
	        $element.removeClass(class1).addClass(class2); 
	      } 
	      else if ($element.hasClass(class2)) { 
	        $element.removeClass(class2).addClass(class1); 
	      } 
	    }); 
	  }; 


	// 可以添加属性的对象方法  
	$.fn.setShadow =  function(opts){
		// 设置初始值
		var defaultValues = {
			copysNum: 5,
            opacity: 0.1
		}

		var options = $.extend(defaultValues, opts); 

		return $(this).each(function() {
			 var $currentObj = $(this);
			 var cpNum = options.copysNum;
			 //console.log("cpNum== " + cpNum+";options.opacity== "+options.opacity);
			 for (var i = cpNum; i >= 0; i--) {
			 	$currentObj
			 	.clone()
			 	.css({
			 		position: 'absolute', 
                    left: $currentObj.offset().left + i, 
                    top: $currentObj.offset().top + i, 
                    margin: 0, 
                    zIndex: -1, 
                    opacity: options.opacity,
                    
			 	})
			 	.appendTo('body');
			 };
		});
	} 

})(jQuery)
